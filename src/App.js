import store from './store.js'
import { Provider } from 'react-redux'
import Main from './components/Main'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'

function App () {
  return (
    <Provider store={store}>
      <Main />
    </Provider>
  )
}

export default App
