/* eslint-disable import/no-anonymous-default-export */
import { get, baseURL } from './base/index'

export default {
  get: (data) => {
    const params = { ...data }
    return get(`${baseURL}/iecho`, { params })
  }
}
