import axios from 'axios'

export const defaultParams = () => ({
  headers: {
    'Content-type': 'application/json'
  }
})

export default axios
