import axios from './axios'
import get from './get'
const baseURL = process.env.REACT_APP_API_URL || 'http://localhost:4000'

export {
  axios,
  get,
  baseURL
}
