import axios, { defaultParams } from './axios'

const get = async (url, params) => await axios.get(url, { ...defaultParams(), ...params })

export default get
