// import appConfigApi from './api/appConfigApi';
import wordApi from './api/wordApi'

const api = {
  word: wordApi
}

export default api
