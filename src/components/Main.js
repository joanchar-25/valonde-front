import React from 'react'
import Search from './Search'
import Result from './Result'

const Main = () => {
  return (
    <>
      <Search />
      <Result />
    </>
  )
}

export default Main
