import React, { useState } from 'react'
import { connect } from 'react-redux'
import { executeInvestText } from '../actions/wordActions'
import { Button, Container, Form } from 'react-bootstrap'
import AlertMessage from './AlertMessage'

const Search = ({ investText }) => {
  const [value, setValue] = useState('')
  const [message, setMessage] = useState({
    show: false,
    title: '',
    text: ''
  })

  const getData = async (event) => {
    event.preventDefault()
    try {
      await investText({
        text: value
      })
    } catch (error) {
      let text = 'an error has occured'
      if (error.response && error.response.status && error.response.status === 400) {
        text = error.response.data.error
      }
      setMessage({
        show: true,
        title: 'Error',
        text
      })
    }
  }

  return (
    <>
      <Container className='search-container'>
        <Form className='d-flex justify-content-center'>
          <Form.Group className='my-2 w-25' controlId='text'>
            <Form.Control type='text' placeholder='Insert Text' value={value} onChange={(event) => setValue(event.target.value)} />
          </Form.Group>
          <div className='ml-3 my-2'>
            <Button variant='primary' type='submit' onClick={(event) => getData(event)}>
              Send
            </Button>
          </div>
        </Form>
      </Container>
      <AlertMessage message={message} setMessage={setMessage} />
    </>
  )
}

const stateToProps = state => ({})

const dispatchToProps = {
  investText: executeInvestText
}

export default connect((stateToProps), dispatchToProps)(Search)
