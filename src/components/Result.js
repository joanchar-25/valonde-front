import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { ListGroup, Container } from 'react-bootstrap'

const Main = ({ data }) => {
  const [listText, setListText] = useState([])

  useEffect(() => {
    if (data && data.word) {
      setListText([data.word, ...listText])
    }
  }, [data])

  return (
    <Container className='w-75 mt-4 pb-5 main-container'>
      <h3 className='ml-4 pt-4 pb-2 font-weight-bold'>Results:</h3>
      {
        listText && listText.length > 0 &&
          <ListGroup className='w-50 m-auto'>
            {
              listText.map((value, index) =>
                <ListGroup.Item key={index} className='text-left d-flex justify-content-between'>
                  <span className='f14'>
                    {value.text}
                  </span>
                  <span className='text-success font-weight-bold f10'>
                    {value.palindrome && 'palindromo'}
                  </span>
                </ListGroup.Item>)
            }
          </ListGroup>
      }
    </Container>
  )
}

const stateToProps = state => ({
  data: state.words.data
})

const dispatchToProps = {}

export default connect(stateToProps, dispatchToProps)(Main)
