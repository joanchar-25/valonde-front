import React from 'react'
import { Container, Toast } from 'react-bootstrap'

const AlertMessage = ({ message, setMessage }) => {
  return (
    <Container className='d-flex justify-content-end mt-3'>
      <Toast
        onClose={() => setMessage({ ...message, show: false })}
        show={message.show}
        delay={2000}
        autohide
      >
        <Toast.Header>
          <img
            src='holder.js/20x20?text=%20'
            className='rounded mr-2'
            alt=''
          />
          <strong className='mr-auto'>{message.title}</strong>
        </Toast.Header>
        <Toast.Body>{message.text}</Toast.Body>
      </Toast>
    </Container>
  )
}
export default AlertMessage
