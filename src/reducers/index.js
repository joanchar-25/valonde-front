import { combineReducers } from 'redux'
// Reducers
import wordReducers from './wordReducers '

export default combineReducers({
  words: wordReducers
})
