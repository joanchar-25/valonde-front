import { createAction } from 'redux-actions'
import api from '../utils/apiClient'

export const fetchDataRequest = createAction('FETCH_DATA_REQUEST')
export const fetchDataSuccess = createAction('FETCH_DATA_SUCCESS')
export const fetchDataFailure = createAction('FETCH_DATA_FAILURE')

export const executeInvestText = (params) => async (dispatch) => {
  try {
    dispatch(fetchDataRequest())
    const response = await api.word.get(params)
    const data = {
      word: response.data
    }
    dispatch(fetchDataSuccess(data))
  } catch (error) {
    dispatch(fetchDataFailure({ error }))
    throw error
  }
}
