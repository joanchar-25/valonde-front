# FRONT API

## Descrición

Front que permite enviar un texto a una API y como resultado mostrar el mismo texto invertido. De igual manera permite identificar si el texto enviando corresponde a un palíndromo (palabra que se lee igual en un sentido que en otro).

## Tecnologías

Esta solución se encuentra implementada en React, soportándose en diferentes paquetes y librerías para completar toda la funcionalidad requerida.

```
React
Redux 
StandarJs
Axios
Bootstrap
```

## Instalación y ejecución

Luego del clonado del repositorio, se puede instalar y ejecutar de dos maneras

### Utilizando NPM

Ejecute el siguiente comando, para realizar la instalación de todos los paquetes y librerías necesarias.

```
npm install
```

Una vez instaladas todas las dependencias, se debe ejecutar el siguiente comando para comenzar la ejecución

```
npm start
```

Por defecto, esto levantara una página en
```
http://localhost:3000
```

### Utilizando docker-compose

También se puede realizar la instalación de todos los paquetes y librerías necesarios através de docker-compose. Para ello se debe asegurar de tener instalado docker y docker-compose en su ambiente.
En caso de no ternerlo instalado, se recomienda leer el siguiente enlace

https://docs.docker.com/

Ejecute el siguiente comando, para realizar la instalación de todos los paquetes y librerías necesarias.

```
docker-compose run --rm front npm install
```

Una vez instaladas todas las dependencias, se debe ejecutar el siguiente comando para comenzar la ejecución

```
docker-compose up
```

Por defecto, esto levantara una página en
```
http://localhost:3000
```

Si se desea validar la correcta ejecución del contenedor Docker, se puede realizar a través del comando

```
docker ps
```

O en su defecto

```
docker-compose ps
```

## Funcionalidad

Esta implementación permite ingresar un texto en un input, el cual mediante una petición a un API, muestra los resultado invertidos de los diferentes textos introducidos, desde el útimo hasta el primero. De igual manera permite identificar si el texto introducido corresponde a un palíndromo.
